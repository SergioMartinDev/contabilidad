import parserExcel from './parserExcel.js';
import { cleanConcepto } from './tools.js';

const datePattern = /(\d{2})\/(\d{2})\/(\d{4})/;

export default async function parserCaixabank2(path) {
  let data = await parserExcel(path, 4);
  data = data
    .filter(value => value[4])
    .map(value => [
      value[4].replace(datePattern, '$3-$2-$1'),
      value[5].replace(datePattern, '$3-$2-$1'),
      cleanConcepto(value[14], value[15], value[16], value[17], value[18], value[19], value[20], value[21], value[22], value[23]),
      value[6] ? Number(value[6]) : -Number(value[7]),
      value[8] ? Number(value[8]) : -Number(value[9])
    ]);
  data.reverse();
  return data;
}
