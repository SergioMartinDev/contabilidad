import parserExcel from './parserExcel.js';
import { cleanConcepto } from './tools.js';

export default async function parserCaixaltea(path) {
  const data = await parserExcel(path, 1);
  return data.map(value => [
    new Date(value[0]),
    new Date(value[1]),
    cleanConcepto(value[2]),
    Number(value[3]),
    Number(value[4])
  ]);
}
