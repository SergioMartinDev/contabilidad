import * as XLSX from 'xlsx';
import * as fs from 'node:fs';
import { Readable } from 'node:stream';
import * as cpexcel from 'xlsx/dist/cpexcel.full.mjs';

import { readFile } from 'node:fs/promises';

XLSX.set_fs(fs);
XLSX.stream.set_readable(Readable);
XLSX.set_cptable(cpexcel);

export default async function parserExcel(path, range) {
  const file = await readFile(path);
  const workbook = XLSX.read(file, { cellDates: true, cellText: false });
  const worksheet = workbook.Sheets[workbook.SheetNames[0]];
  const data = XLSX.utils.sheet_to_json(worksheet, { header: 1, range, raw: false, UTC: true, dateNF: 'yyyy-mm-dd' });
  return data;
}
