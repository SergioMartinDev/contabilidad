export function cleanConcepto(...conceptoArray) {
  const conceptoStr = conceptoArray
    .reduce((previous, current) => {
      return previous.concat(current.split('\n'));
    }, [])
    .map(val => val.trim().replace(/\s{2,}/g,' '))
    .filter(val => val)
    .reduce((previous, current) => {
      if (previous) previous += '\n';
      previous += current;
      return previous;
    }, '');
  return conceptoStr ? conceptoStr : null;
}
