import parserExcel from './parserExcel.js';
import { cleanConcepto } from './tools.js';

export default async function parserCaixabank(path) {
  let data = await parserExcel(path, 3);
  data = data.map(value => [
    new Date(value[0]),
    new Date(value[1]),
    cleanConcepto(value[2], value[3]),
    Number(value[4]),
    Number(value[5])
  ]);
  data.reverse();
  return data;
}
