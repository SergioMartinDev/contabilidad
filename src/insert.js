import { program } from 'commander';
import pg from 'pg';
import parserCaixabank from './lib/parserCaixabank.js';
import parserCaixabank2 from './lib/parserCaixabank2.js';
import parserCaixaltea from './lib/parserCaixaltea.js';

program
  .name('insert')
  .description('Añade movimientos bancarios a la base de datos de Contabilidad')
  .argument('<path>', 'ruta a archivo de movimientos bancarios')
  .option('-p, --parser <string>', 'parser a emplear: ing, caixabank, caixabank2, caixaltea', 'ing')
  .option('-a, --accountid <id>', 'cuenta bancaria')
  .option('-f, --from <date>', 'desde la fecha')
  .option('-t, --to <date>', 'hasta la fecha')
  .action(async (path, options) => {

    const parser = options.parser;
    const accountId = options.accountid;
    const from = options.from ? new Date(options.from) : undefined;
    const to = options.to ? new Date(options.to) : undefined;
    let data;

    if (parser === 'caixabank') data = await parserCaixabank(path);
    else if (parser === 'caixabank2') data = await parserCaixabank2(path);
    else if (parser === 'caixaltea') data = await parserCaixaltea(path);
    else console.log(`No existe el parser: ${ parser }`);
    console.log(`Movimientos parseados: ${ data.length }`);

    if (from || to) {
      data = data.filter(value => {
        const date = value[0];
        if (from && date.getTime() < from.getTime()) return false;
        if (to && date.getTime() > to.getTime()) return false;
        return true;
      });
    }
    console.log(`Movimientos filtrados: ${ data.length }`);

    const client = new pg.Client({ host: '/var/run/postgresql', database: 'contabilidad' });
    await client.connect();

    const text = 'SELECT COUNT(*) FROM movimientos WHERE id_cuenta = $1 AND fecha >= $2 AND fecha <= $3';
    const values = [accountId, data[0][0], data[data.length - 1][0]];
    const res = await client.query(text, values);
    const duplicatedRows = Number(res.rows[0].count);

    if (duplicatedRows) console.log('Fechas duplicadas');
    else {
      for (let index = 0; index < data.length; index++) {
        const text = 'INSERT INTO movimientos VALUES (DEFAULT, $1, NULL, NULL, $2, $3, $4, $5, $6)';
        const values = [accountId, data[index][0], data[index][1], data[index][2], data[index][3], data[index][4]];
        await client.query(text, values);
      }
      console.log(`Movimientos introducidos: ${ data.length }`);
    }

    await client.end();

  });

program.parse();
